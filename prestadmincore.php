<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once _PS_MODULE_DIR_.'prestadmincore/inc/Settings.php';
require_once _PS_MODULE_DIR_.'prestadmincore/inc/HookSender.php';

class PrestAdminCore extends Module
{
    public function __construct()
    {
        $this->name = 'prestadmincore';
        $this->tab = 'prestadmin';
        $this->version = '0.1';
        $this->author = 'Christopher Morton';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('PrestAdmin Core');
        $this->description = $this->l('Fundamental PrestAdmin utilities and features');

        $this->confirmUninstall = $this->l('Please deregister in PrestAdmin first');
    }

    public function hookActionOrderStatusPostUpdate($params)
    {
        HookSender::send($params);
    }

    /**
     * Install.
     *
     * @return bool
     */
    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }

        if (parent::install()
            && $this->initConfiguration()
            && $this->installTabs()
            && $this->runAllModelsStaticTableMethod('createTable')
            && $this->registerHooks()
        ) {
            return true;
        }

        return false;
    }

    /**
     * Uninstall.
     *
     * @return bool
     */
    public function uninstall()
    {
        if (parent::uninstall()
            && $this->uninstallTabs()
            && $this->removeConfiguration()
            && $this->runAllModelsStaticTableMethod('dropTable')
        ) {
            return true;
        }

        return false;
    }

    /**
     * Rund all models static table method.
     *
     * @param string $methodName
     *
     * @return bool
     */
    private function runAllModelsStaticTableMethod(string $methodName): bool
    {
        foreach (Settings::getModelClassNames() as $class) {
            if (!$this->runStaticTableMethod($class, $methodName)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Run static table method.
     *
     * Runs an ObjectModel's table function
     *
     * @param string $class
     * @param string $methodName
     *
     * @return bool
     */
    private function runStaticTableMethod(string $class, string $methodName): bool
    {
        if (method_exists($class, $methodName)
            && $class::$methodName()
        ) {
            return true;
        }

        return false;
    }

    /**
     * Install tabs.
     *
     * @return bool
     */
    private function installTabs(): bool
    {
        foreach (Settings::getAdminTabs() as $definedTab) {
            $tab = new Tab();
            $tab->active = 1;
            $tab->class_name = $definedTab['className'];
            $tab->name = array();
            foreach (Language::getLanguages(true) as $lang) {
                $tab->name[$lang['id_lang']] = $definedTab['name'];
            }

            $tab->id_parent = is_int($definedTab['parentClassName'])
                ? $definedTab['parentClassName']
                : (int) Tab::getIdFromClassName($definedTab['parentClassName']);
            $tab->module = $this->name;
            if (!$tab->add()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Uninstall tabs.
     *
     * @return bool
     */
    private function uninstallTabs(): bool
    {
        if (version_compare(_PS_VERSION_, '1.7.1.0', '>=')) {
            $this->tabs = Settings::getAdminTabs();

            return true;
        }
        foreach (Settings::getAdminTabs() as $definedTab) {
            if ($idTab = (int) Tab::getIdFromClassName($definedTab['className'])) {
                $tab = new Tab($idTab);
                if (!$tab->delete()) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Register hooks.
     *
     * @return bool
     */
    private function registerHooks(): bool
    {
        foreach (Settings::getHooks() as $hook) {
            if (!$this->registerHook($hook)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Init configuration.
     *
     * @return bool
     */
    private function initConfiguration(): bool
    {
        foreach (Settings::getDefaultConfig() as $key => $value) {
            if (!Configuration::updateValue($key, $value)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Remove configuration.
     *
     * @return bool
     */
    private function removeConfiguration(): bool
    {
        foreach (Settings::getDefaultConfig() as $key => $value) {
            if (!Configuration::deleteByName($key)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get content.
     *
     * Redirect module config page to PrestAdmin Configuration admin tab
     */
    public function getContent()
    {
        Tools::redirectAdmin($this->context->link->getAdminLink('AdminPrestAdminConfig'));
    }
}
