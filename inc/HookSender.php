<?php

use GuzzleHttp\Client;
use GuzzleHttp\Promise;

require_once _PS_MODULE_DIR_.'prestadmincore/vendor/autoload.php';
require_once _PS_MODULE_DIR_.'prestadmincore/classes/PrestAdminInstall.php';

class HookSender
{
    public static function send($data, $endPoint = ''): bool
    {
        $success = true;

        $client = new Client([
            'timeout' => 2.0,
            'body' => json_encode($data),
            'http_errors' => false,
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ]);

        $promises = [];
        $installs = PrestAdminInstall::getAllRegistered();
        foreach ($installs as &$install) {
            $promises[$install['name']] =
                $client->postAsync($install['url'].$endPoint);
        }

        $results = Promise\settle($promises)->wait();

        foreach ($results as $name => $result) {
            if ($result['state'] !== 'fulfilled') {
                $success = false;
                continue;
            }
            $code = $result['value']->getStatusCode();
            if ($code < 200 || $code >= 400) {
                $success = false;
            }
        }

        return $success;
    }
}
