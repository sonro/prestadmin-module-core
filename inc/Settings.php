<?php

class Settings
{
    private static $defaultConfig = [
        'PA_HOOKS_ENABLED' => 1,
    ];

    private static $modelClassNames = [
        'PrestAdminInstall',
    ];

    private static $hooks = [
        'actionOrderStatusPostUpdate',
        'displayFooter',
        'displayOrderConfirmation',
    ];

    /**
     * Get admin tabs.
     *
     * List of back office tabs to install [
     *      'className',
     *      'name',
     *      'visibale',
     *      'icon',
     *      'parentClassName'
     * ]
     *
     * @return array
     */
    public static function getAdminTabs(): array
    {
        $rootNode = version_compare(_PS_VERSION_, '1.7.1.0', '>=') ? 'AdminAdvancedParameters' : 0;

        $definedTabs = array(
            array(
                'className' => 'AdminPrestAdmin',
                'name' => 'PrestAdmin',
                'visible' => 1,
                'icon' => 'flash_on',
                'parentClassName' => $rootNode,
            ),
            array(
                'className' => 'AdminPrestAdminConfig',
                'name' => 'Configuration',
                'visible' => 1,
                'parentClassName' => 'AdminPrestAdmin',
            ),
            array(
                'className' => 'AdminPrestAdminInstall',
                'name' => 'Installs',
                'visible' => 1,
                'parentClassName' => 'AdminPrestAdmin',
            ),
        );

        return $definedTabs;
    }

    /**
     * Get model class names.
     *
     * @return string[]
     */
    public static function getModelClassNames(): array
    {
        return self::$modelClassNames;
    }

    /**
     * Get default config.
     *
     * @return array
     */
    public static function getDefaultConfig(): array
    {
        return self::$defaultConfig;
    }

    /**
     * Get hooks.
     *
     * @return array
     */
    public static function getHooks(): array
    {
        return self::$hooks;
    }
}
