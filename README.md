#PrestAdmin Core Prestashop Module

Core connection and functionality with PrestAdmin

===

###Features

- Connect to multiple PrestAdmin installations
- Hook into order updates to send real time data
- Provide a RESTful API more feature rich than PS Webservice