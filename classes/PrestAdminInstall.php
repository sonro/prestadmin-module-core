<?php

class PrestAdminInstall extends ObjectModel
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var bool
     */
    public $passwd;

    /**
     * @var string
     */
    public $url;

    /**
     * @var bool
     */
    public $registered = false;

    public static $definition = [
        'table' => 'prestadmin_install',
        'primary' => 'id_prestadmin_install',
        'multilang' => false,
        'fields' => [
            'name' => [
                'type' => self::TYPE_STRING,
                'validate' => 'isGenericName',
                'required' => true,
                'size' => 50,
            ],
            'passwd' => [
                'type' => self::TYPE_STRING,
                'validate' => 'isPasswdAdmin',
                'required' => true,
                'size' => 180,
            ],
            'url' => [
                'type' => self::TYPE_STRING,
                'validate' => 'isAbsoluteUrl',
                'required' => true,
                'size' => 180,
            ],
            'registered' => [
                'type' => self::TYPE_BOOL,
                'validate' => 'isBool',
                'required' => true,
            ],
        ],
    ];

    public function register(): bool
    {
        $this->registered = true;

        return true;
    }

    public static function getAll()
    {
        $sql =
            'SELECT `name`, `url` FROM `'
            ._DB_PREFIX_.self::$definition['table'].'`';

        return Db::getInstance()->executeS($sql);
    }

    public static function getAllRegistered()
    {
        $sql =
            'SELECT `name`, `url` FROM `'
            ._DB_PREFIX_.self::$definition['table'].'`
            WHERE `registered`=1';

        return Db::getInstance()->executeS($sql);
    }

    public static function createTable()
    {
        $sql =
            'CREATE TABLE IF NOT EXISTS `'
            ._DB_PREFIX_.self::$definition['table'].'` (
                `id_prestadmin_install` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                `name` VARCHAR(50) NOT NULL UNIQUE,
                `passwd` VARCHAR(180) NOT NULL,
                `url` VARCHAR(180) NOT NULL,
                `registered` TINYINT NOT NULL,
                PRIMARY KEY (`id_prestadmin_install`)
            ) DEFAULT CHARSET=utf8';

        return Db::getInstance()->execute($sql);
    }

    public static function dropTable()
    {
        $sql =
            'DROP TABLE IF EXISTS `'
            ._DB_PREFIX_.self::$definition['table'].'`';

        return Db::getInstance()->execute($sql);
    }
}
