<?php

require_once _PS_MODULE_DIR_.'prestadmincore/classes/PrestAdminInstall.php';

/**
 * @property PrestAdminInstall $object
 */
class AdminPrestAdminInstallController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->context = Context::getContext();
        $this->table = 'prestadmin_install';
        $this->className = 'PrestAdminInstall';

        $title = $this->l('PrestAdmin Installs');
        $this->page_header_toolbar_title = $title;
        $this->meta_title = $title;

        $this->addRowAction('edit');
        $this->addRowAction('delete');

        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?'),
                'icon' => 'icon-trash',
            ),
        );

        $this->fields_list = array(
            'id_prestadmin_install' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
            ),
            'name' => array(
                'title' => $this->l('Name'),
            ),
            'url' => array(
                'title' => $this->l('URL'),
            ),
        );

        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('PrestAdmin Installs'),
                'icon' => 'icon-mail',
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Name'),
                    'name' => 'name',
                    'size' => 50,
                    'required' => true,
                ),
                array(
                    'type' => 'password',
                    'label' => $this->l('Module Api Password'),
                    'name' => 'passwd',
                    'required' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('URL'),
                    'name' => 'url',
                    'required' => true,
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
            ),
        );

        $this->fields_value['api_key'] = false;

        parent::__construct();
    }

    public function initPageHeaderToolbar()
    {
        if (empty($this->display)) {
            $this->page_header_toolbar_btn['new_prestadmin_install'] = array(
                'href' => self::$currentIndex.'&addprestadmin_install&token='.$this->token,
                'desc' => $this->l('Add new PrestAdmin install'),
                'icon' => 'process-icon-new',
            );
        }

        parent::initPageHeaderToolbar();
    }

    public function processSave()
    {
        parent::processSave();
    }
}
