<?php

class AdminPrestAdminConfigController extends ModuleAdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->bootstrap = true;
        $this->context = Context::getContext();
        $this->className = 'Configuration';
        $this->table = 'configuration';

        $title = $this->l('PrestAdmin Configuration');
        $this->page_header_toolbar_title = $title;
        $this->meta_title = $title;

        $fields = $this->getFields();

        $this->fields_options = array(
            'prestAdminGeneral' => array(
                'title' => $this->l('General'),
                'icon' => 'icon-cogs',
                'fields' => $fields,
                'submit' => array('title' => $this->l('Save')),
            ),
        );
    }

    private function getFields()
    {
        return [
            'PA_HOOKS_ENABLED' => [
                'title' => $this->l('Enable Hooks'),
                'desc' => $this->l('Enable or disable sending live data to PrestAdmin'),
                'validation' => 'isBool',
                'cast' => 'intval',
                'type' => 'bool',
                'default' => '0',
                'visibility' => Shop::CONTEXT_ALL,
            ],
        ];
    }
}
